<%@ page import="pl.sda.jsp.model.Reservation" %>
<%@ page import="java.util.List" %>
<html>
<body>
<%@include file="navigator.jsp" %>
<h2>Hello World!</h2>

<p>Formularz rezerwacji!</p>

<%
    String reservation_id = request.getParameter("reservationId");
    String event = "", name = "", surname = "";
    try {
        Integer id = Integer.parseInt(reservation_id);
        List<Reservation> reservationList =
                (List<Reservation>) session.getAttribute("reservations");

        for (int i = 0; i < reservationList.size(); i++) {
            if (reservationList.get(i).getId() == id) {
                event = reservationList.get(i).getEventName();
                name = reservationList.get(i).getParticipantName().split(" ")[0];
                surname = reservationList.get(i).getParticipantName().split(" ")[1];
                break;
            }
        }
    } catch (NumberFormatException | ClassCastException cce) {
        event = "";
        name = "";
        surname = "";
    }
%>

<form action="dodaj_rezerwacje.jsp" method="post">
    <input type="text" id="reservationId" name="reservationId" value="<%= reservation_id %>" hidden>
    <form-group>
        <div><label for="event">Nazwa Eventu:</label></div>
        <div><input type="text" id="event" name="event" value="<%= event %>"></div>
    </form-group>
    <form-group>
        <div><label for="name">Imie:</label></div>
        <div><input type="text" id="name" name="name" value="<%= name %>"></div>
    </form-group>
    <form-group>
        <div><label for="surname">Nazwisko:</label></div>
        <div><input type="text" id="surname" name="surname" value="<%= surname %>"></div>
    </form-group>
    <form-group>
        <div><label for="reservation_time">:</label></div>
        <div><input type="datetime-local" id="reservation_time" name="reservation_time"></div>
    </form-group>
    <input type="submit" value="Przeslij formularz!">
</form>

</body>
</html>