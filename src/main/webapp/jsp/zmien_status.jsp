<%@ page import="pl.sda.jsp.model.Reservation" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/23/18
  Time: 8:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String id_usuwanego = request.getParameter("reservationId");
    Integer id_zmieniane = Integer.parseInt(id_usuwanego);

    // pobieram i rzutuje do listy
    List<Reservation> reservationList =
            (List<Reservation>) session.getAttribute("reservations");

    // szukamy i usuwamy wybraną rezerwację
    for (int i = 0; i < reservationList.size(); i++) {
        if (reservationList.get(i).getId() == id_zmieniane) {
            reservationList.get(i).setConfirmed(!reservationList.get(i).isConfirmed());
            // po znalezieniu (i zmianie statusu rezerwacji) przerywamy pętle
            break;
        }
    }

    session.setAttribute("reservations", reservationList);
    response.sendRedirect("lista_rezerwacji.jsp");
%>
</body>
</html>
