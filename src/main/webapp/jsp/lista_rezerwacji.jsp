<%@ page import="pl.sda.jsp.model.Reservation" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/18
  Time: 8:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista Rezererwacji</title>
</head>
<body>
<%@include file="navigator.jsp" %>
<table style="border: 1px solid #000">
    <tr>
        <td>Id</td>
        <td>Event</td>
        <td>Reservation time</td>
        <td>Participant name</td>
        <td>Confirmed</td>
        <td>Actions</td>
    </tr>
    <%
        // sprawdzamy czy w sesji jest gdzieś lista rezerwacji
        Object reservations = session.getAttribute("reservations");
        // weryfikujemy czy rezerwacja istnieje i czy jest listą
        if (reservations != null) {
            // rzutujemy obiekt z sesji i zamieniamy go na obiekt listy
            List<Reservation> reservationList = (List<Reservation>) reservations;

            for (Reservation reservation : reservationList) {
                out.print("<tr style=\"border: 1px solid #000>\">");
                out.print("<td>" + reservation.getId() + "</td>");
                out.print("<td>" + reservation.getEventName() + "</td>");
                out.print("<td>" + reservation.getTime() + "</td>");
                out.print("<td>" + reservation.getParticipantName() + "</td>");
                out.print("<td>" + reservation.isConfirmed() + "</td>");
                out.print("<td> " + generateRemoveLink(reservation.getId()) + "</td>");
                out.print("<td> " + generateEditLink(reservation.getId()) + "</td>");
                out.print("<td> " + generateConfirmLink(reservation.getId(), reservation.isConfirmed()) + "</td>");
                out.print("</tr>");
            }
        }
    %>
</table>
</body>
</html>
<%!
    private String generateEditLink(long id) {
        return "<a href=\"formularz_rezerwacji.jsp?reservationId=" + id + "\">Edytuj</a>";
    }

    private String generateConfirmLink(long id, boolean confirmed) {
        if (!confirmed) {
            return "<a href=\"zmien_status.jsp?reservationId=" + id + "\">Potwierdz</a>";
        } else {
            return "<a href=\"zmien_status.jsp?reservationId=" + id + "\">Anuluj potwierdzenie</a>";
        }
    }

    private String generateRemoveLink(long id) {
        String link = "<a href=\"usun_rezerwacje.jsp?reservationId=" + id + "\">Usun</a>";
        return link;
    }
%>