package pl.sda.jsp.servlets;

import pl.sda.jsp.dao.ReservationDao;
import pl.sda.jsp.model.Reservation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addReservation")
public class FormServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("servlets/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Reservation reservation = new Reservation();
        reservation.setEventName(req.getParameter("event"));
        reservation.setParticipantName(
                req.getParameter("name") + " " + req.getParameter("surname"));

        ReservationDao.saveReservation(reservation);

        resp.sendRedirect("/reservations");
    }
}
