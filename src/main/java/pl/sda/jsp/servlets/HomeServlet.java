package pl.sda.jsp.servlets;

import pl.sda.jsp.dao.ReservationDao;
import pl.sda.jsp.model.Reservation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/reservations")
public class HomeServlet extends HttpServlet {

    public int counter = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Reservation> list = ReservationDao.getAllReservations();
// 1. Pobrać listę rezerwacji z DAO
        req.setAttribute("reservationList", list);
// 2. Dodać listę rezerwacji jako
// atrybut pod nazwą "reservationList"
// podobnie jak niżej
        req.setAttribute("counter", String.valueOf(counter++));

        RequestDispatcher dispatcher = req.getRequestDispatcher("servlets/view.jsp");
        dispatcher.forward(req, resp);
    }
}
