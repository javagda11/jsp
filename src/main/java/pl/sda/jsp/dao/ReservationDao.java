package pl.sda.jsp.dao;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import pl.sda.jsp.model.Reservation;

import java.util.List;

public class ReservationDao {

    public static void saveReservation(Reservation reservation) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(reservation);

            transaction.commit();
        } catch (SessionException e) {
            transaction.rollback();
        }
    }

    public static List<Reservation> getAllReservations() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        List<Reservation> list =
                session.createQuery("from Reservation ", Reservation.class).list();

        return list;
    }
}
