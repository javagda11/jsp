<%@ page import="pl.sda.jsp.model.Reservation" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/18
  Time: 8:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%! int reservationCounter = 0; %>

<%
    String event = request.getParameter("event");
    String reservation_id = request.getParameter("reservationId");
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    String reservationTime = request.getParameter("reservation_time");


    // sprawdzamy czy w sesji jest gdzieś lista rezerwacji
    Object reservations = session.getAttribute("reservations");

    // weryfikujemy czy rezerwacja istnieje i czy jest listą
    List<Reservation> reservationList;
    if (reservations != null && reservations instanceof List) {
        // rzutujemy obiekt z sesji i zamieniamy go na obiekt listy
        reservationList = (List<Reservation>) reservations;
    } else {
        // jeżeli nie ma listy to tworzymy pustą
        reservationList = new ArrayList<>();
    }

    Reservation reservation = null;
    if (reservation_id != null && !reservation_id.equals("null")) {
        Integer id_zmieniane = Integer.parseInt(reservation_id);
        // edytujemy rezerwację
        for (int i = 0; i < reservationList.size(); i++) {
            if (reservationList.get(i).getId() == id_zmieniane) {
                reservation = reservationList.remove(i);

                reservation.setEventName(event);
                reservation.setParticipantName(name + " " + surname);

                break;
            }
        }
    } else {
        // nie edytujemy rezerwacji
        // dodajemy nową - tworzę ją
        reservation = new Reservation(
                reservationCounter++,
                LocalDateTime.now(),
                event,
                name + " " + surname,
                false);
    }

    // dodajemy do listy nową rezerwacje
    reservationList.add(reservation);

    // zapisujejmy listę (z nową rezerwacją) z powrotem w sesji
    session.setAttribute("reservations", reservationList);


    response.sendRedirect("lista_rezerwacji.jsp");
%>

</body>
</html>
