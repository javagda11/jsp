<html>
<body>
<%@include file="navigator.jsp"%>

<h2>Hello World!</h2>
<%! int counter = 0; %>

<p>Dodam paragraf!</p>
<p><% out.println("Licznik" + (counter++)); %></p>


<p>Tabela 1:</p>
<table style="border: solid 1px red">
    <tr>
        <%
            String mnoznik = request.getParameter("mnoznik");

            if (mnoznik != null && !mnoznik.isEmpty()) {
                int mnoznikInt = Integer.parseInt(mnoznik);

                for (int i = 0; i < 10; i++) {
                    out.print("<td>" + (mnoznikInt * i) + "</td>");
                }
            } else {
                out.print("<td>Brak parametru!</td>");
            }
        %>
    </tr>
</table>

<p>Tabela 2:</p>
<%-- Wypisz tabliczkę mnożenia w tabelce--%>
<table style="border: solid 1px red">
    <%
        for (int i = 1; i < 10; i++) {
            out.print("<tr> ");
            for (int j = 1; j < 10; j++) {
                out.print("<td>" + (i * j) + "</td>");
            }
            out.print("</tr>");
        }
    %>
</table>

<p>Tabela 3 (wymiar):</p>
<table style="border: solid 1px red">
    <%
        String wymiar = request.getParameter("wymiar");

        if (wymiar != null && !wymiar.isEmpty()) {
            int wymiarInt = Integer.parseInt(wymiar);
            for (int i = 1; i < wymiarInt; i++) {
                out.print("<tr> ");
                for (int j = 1; j < wymiarInt; j++) {
                    out.print("<td>" + (i * j) + "</td>");
                }
                out.print("</tr>");
            }
        }
    %>
</table>

Formularz 1 (mnożenie 1) :

<form action="index.jsp" method="post">
    <input type="number" id="mnoznik" name="mnoznik">
    <input type="submit" value="Wymnóż">
</form>

Formularz 2 (mnożenie konfigurowalna tabliczka) :

<form action="index.jsp" method="post">
    <form-group>
        <div><label for="wymiar">Wymiar</label></div>
        <div><input type="number" id="wymiar" name="wymiar"></div>
    </form-group>
    <input type="submit" value="Wymnóż wymiar">
</form>

</body>
</html>