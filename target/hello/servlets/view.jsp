<%@ page import="pl.sda.jsp.model.Reservation" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View</title>
</head>
<body>
<h2>Hello dudes!</h2>

<%-- Wyświetlić listę requestów --%>
<table style="border: 1px solid #000">
    <tr>
        <td>Id</td>
        <td>Event</td>
        <td>Reservation time</td>
        <td>Participant name</td>
        <td>Confirmed</td>
        <td>Actions</td>
    </tr>
    <%
        List<Reservation> reservationList =
                (List<Reservation>) request.getAttribute("reservationList");

        for (Reservation reservation : reservationList) {
            out.print("<tr style=\"border: 1px solid #000>\">");
            out.print("<td>" + reservation.getId() + "</td>");
            out.print("<td>" + reservation.getEventName() + "</td>");
            out.print("<td>" + reservation.getTime() + "</td>");
            out.print("<td>" + reservation.getParticipantName() + "</td>");
            out.print("<td>" + reservation.isConfirmed() + "</td>");
//            na link ma zostać wywołane zapytanie na adres /status?reservationId=??id
//            należy stworzyć servlet (nowy) (/status) - który ma metodę doGet
//              - w tej metodzie wyciągamy z requestu parametr "reservationId"
//              - szukamy rezerwacji w bazie danych,
//              - zmieniamy jej status,
//              - zapisujemy rezerwacje i
//              - przekierowujemy na
//                                  /reservations
            out.print("</tr>");
        }

    %>
</table>
<c:out value="${counter}"/>
</body>
</html>
